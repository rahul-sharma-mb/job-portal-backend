const express = require("express");
const dotenv = require("dotenv");
const app = express();

//config file
dotenv.config({
  path: "./config/config.env",
});

//connect to DB
const connectToDb = require("./config/database");
connectToDb();

//to parse the incoming req else will get undefined
app.use(express.json());

//routes
const jobRoutes = require("./routes/jobs");
app.use("/api/v1", jobRoutes);

app.use((req, res, next) => {
  res
    .status(404)
    .json({
      message: "404 not found",
    })
    .end();
});

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).json({
    message:
      "An error has occurred due to internal server error.Try again later",
  });
});

app.listen(process.env.PORT, () => {
  console.log(
    "listening at http://localhost:" +
      process.env.PORT +
      " in " +
      process.env.NODE_ENV +
      " mode"
  );
});
