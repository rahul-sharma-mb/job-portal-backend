const router = require("express").Router();
const { getJobs, createJob } = require("../controller/jobsController");

router.route("/jobs").get(getJobs);
router.route("/job/new").post(createJob);

module.exports = router;
