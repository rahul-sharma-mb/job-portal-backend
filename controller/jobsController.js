let Job = require("../models/jobs");
//get all jobs => api/v1/jobs
exports.getJobs = async (req, res, next) => {
  let allJobs = await Job.find();
  res.status(200).json({
    "no of jobs": allJobs.length,
    jobs: allJobs,
  });
};

//create a new job => api/v1/job/new
exports.createJob = async (req, res, next) => {
  ////$$ newly learnt//

  //let job = await new Job(req.body);
  let job = await Job.create(req.body);

  job
    .save()
    .then(() => {
      console.log("saved");
    })
    .catch((err) => {
      console.log("err", err);
    });

  res.status(200).json("ok");
};
