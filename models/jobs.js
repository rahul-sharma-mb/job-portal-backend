const mongoose = require("mongoose");
const validator = require("validator");
const slugify = require("slugify");
const geoCoder = require("../utils/geocoder");
const jobsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "please provide a title"],
    trim: true,
    maxlength: [100, "title cannot exceed 100 characters"],
  },

  // $$newly learnt
  slug: String,
  description: {
    type: String,
    required: [true, "please provide a description"],
    maxlength: [1000, "desc cannot exceed 100 characters"],
  },
  email: {
    type: String,
    required: [true, "please provide an email address"],
    validate: [validator.isEmail, "please provide a correct email address"],
  },
  address: {
    type: String,
    required: [true, "Please add an address"],
  },

  //$$newly learnt-- for geocoder package
  location: {
    type: {
      type: String,
      enum: ["Point"],
    },
    coordinates: {
      type: [Number],
      index: "2dphere",
    },
    formattedAddress: String,
    city: String,
    state: String,
    zipcode: String,
    country: String,
  },
  company: {
    type: String,
    required: [true, "Please add a company name"],
  },
  industry: {
    type: [String],
    required: true,
    enum: {
      values: [
        "Business",
        "Information technology",
        "Banking",
        "Education/Training",
        "Telecommunications",
        "Others",
      ],
      message: "Please select a value",
    },
  },
  jobType: {
    type: String,
    required: [true, "please provide a job type"],
    // $$newly learnt
    enum: {
      values: ["internship", "fulltime", "contract"],
      message: "Please select correct option type",
    },
  },
  minEducation: {
    type: String,
    required: true,
    enum: {
      values: ["Bachelors", "Master", "PHD"],
      message: "Please select a correct option for education",
    },
  },
  positions: {
    type: Number,
    default: 1,
  },
  experience: {
    type: String,
    required: true,
    enum: {
      values: ["fresher", "1 year - 2 year", "2 year - 3 year", "3 year +"],
      message: "Please select your experience",
    },
  },
  salary: {
    type: Number,
    required: [true, "please enter the expected salary"],
  },
  postingDate: {
    type: Date,
    default: Date.now,
  },
  lastDate: {
    // $$newly learnt
    type: Date,
    // $$newly learnt
    default: new Date().setDate(new Date().getDate() + 7),
  },
  applicantApplied: {
    type: [Object],
    // $$newly learnt
    select: false,
  },
});

//$$ newly learnt
//creating slug for the title,middleware
//es6 functions dont work
jobsSchema.pre("save", function (next) {
  //   console.log(this);

  this.slug = slugify(this.title);
  next();
});

//saving location
jobsSchema.pre("save", async function (next) {
  let loc = await geoCoder.geocode(this.address);
  this.location = {
    type: "Point",
    coordinates: [loc[0].longitude, loc[0].latitude],
    formattedAddress: loc[0].formattedAddress,
    city: loc[0].city,
    state: loc[0].stateCode,
    zipcode: loc[0].zipCode,
    country: loc[0].countryCode,
  };

  next();
});

module.exports = mongoose.model("Job", jobsSchema);
