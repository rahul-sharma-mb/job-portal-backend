const mongoose = require("mongoose");

const connectToDB = () => {
  //console.log(process.env.MONGODB_URI);
  mongoose
    .connect(process.env.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    .then(() => {
      console.log("connected to db");
    })
    .catch((e) => {
      console.log(e);
    });
};
module.exports = connectToDB;
